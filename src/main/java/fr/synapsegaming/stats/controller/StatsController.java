package fr.synapsegaming.stats.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.synapsegaming.commons.controller.AbstractController;
import fr.synapsegaming.stats.service.StatsService;
import fr.synapsegaming.user.entity.Clazz;



@Controller("StatsController")
@SessionAttributes(value = { "user", "userResources" })
@RequestMapping("/stats")
public class StatsController extends AbstractController {
	
	private static final String TOP_FIVE_CLAZZ = "topFiveClazz";
	@Autowired
	private StatsService statsService;
	private static final String STATS_VIEW_NAME = "Stats";
	
	@RequestMapping(value="",method = RequestMethod.GET)
    public ModelAndView stat(){
    	page = new ModelAndView(STATS_VIEW_NAME);
    	page.addObject(TOP_FIVE_CLAZZ, statsService.getFiveClazzMostPlayed());
		return page;	
    }

}

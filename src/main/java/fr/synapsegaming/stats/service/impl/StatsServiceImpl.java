package fr.synapsegaming.stats.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.synapsegaming.stats.service.StatsService;
import fr.synapsegaming.user.dao.ClazzDao;
import fr.synapsegaming.user.entity.Clazz;

@Service("StatsService")
@Transactional
public class StatsServiceImpl implements StatsService {

	private static final int NUMBER_OF_MOST_PLAYED_CLASSES = 5;
	@Autowired
	private ClazzDao clazzDao;
	
	@Override
	public List<Clazz> getFiveClazzMostPlayed() {
		return clazzDao.listMostPlayedClasses(NUMBER_OF_MOST_PLAYED_CLASSES);
	}

}

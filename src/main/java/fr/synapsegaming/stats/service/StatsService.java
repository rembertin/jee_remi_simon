package fr.synapsegaming.stats.service;

import java.util.List;

import fr.synapsegaming.user.entity.Clazz;

public interface StatsService {
	
	public List<Clazz> getFiveClazzMostPlayed();
	

}

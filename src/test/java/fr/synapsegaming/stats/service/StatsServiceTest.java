package fr.synapsegaming.stats.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fr.synapsegaming.commons.controller.AbstractControllerTest;
import fr.synapsegaming.user.entity.Clazz;

public class StatsServiceTest extends AbstractControllerTest{
	@Autowired
	private StatsService statsService;
	
	private List<Clazz> clazzs;
	
	@Before
	public void initialize() {
		clazzs = statsService.getFiveClazzMostPlayed();
	}
	
	@Test
	public void testMostUsedClazzIsNotNull() {
		assertNotNull(clazzs);
	}
	
	@Test
	public void testMostUsedClassesContainsFiveClazz() {
		assertTrue(clazzs.size() == 5);
	}

	@Test
	public void testFirstUsedClazz() {
		assertTrue(clazzs.get(0).getId() == 3);
	}
	
	@Test
	public void testSecondUsedClazz() {
		assertTrue(clazzs.get(1).getId() == 5);
	}
	
	@Test
	public void testThirdUsedClazz() {
		assertTrue(clazzs.get(2).getId() == 4);
	}
	
	@Test
	public void testFourthUsedClazz() {
		assertTrue(clazzs.get(3).getId() == 2);
	}
	
	@Test
	public void testFifthUsedClazz() {
		assertTrue(clazzs.get(4).getId() == 1);
	}
}

package fr.synapsegaming.stats.controller;

import org.junit.Test;

import fr.synapsegaming.commons.controller.AbstractControllerTest;

public class StatsControllerTest extends AbstractControllerTest {
	
	 private static final String STATS_VIEW = "Stats";

	 private static final String STATS_ROUTE = "/stats";
	 
	 private static final String GET_HTTP_METHOD = "GET";
	
	@Test
	public void testStatsRoute() throws Exception{
		testRoute(STATS_ROUTE, STATS_VIEW, GET_HTTP_METHOD);
	}
	

}
